import React from "react";
import "./page.css";

import { Amplify } from 'aws-amplify';
import { withAuthenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import awsExports from '../aws-exports';
Amplify.configure(awsExports);

const Error404page = ({ signOut, user }) => {
  {
    return (
        <div className="wrapper">
            <image src="images/ErrorPage404.jpg" width="100%" height="100%" />
        </div>
    )
  }
};

export default withAuthenticator(Error404page);
