import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Carbonpage from "./pages/Carbonpage";
import Materialpage from "./pages/Materialpage";
import Temperaturepage from "./pages/Temperaturepage";
import Energypage from "./pages/Energypage"
import Error404page from "./pages/404page";

import { Amplify } from 'aws-amplify';
import { withAuthenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import awsExports from './aws-exports';
Amplify.configure(awsExports);

function App({ signOut, user }) {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Carbonpage signOut={signOut} user={user} />} />
          <Route path="/material" element={<Materialpage signOut={signOut} user={user} />} />
          <Route path="/temperature" element={<Temperaturepage signOut={signOut} user={user} />} />
          <Route path="/energy" element={<Energypage signOut={signOut} user={user} />} />
          <Route path="/404" element={<Error404page signOut={signOut} user={user} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default withAuthenticator(App);
